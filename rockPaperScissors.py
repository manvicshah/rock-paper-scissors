import random

options = ["rock", "paper", "scissors"]


def generate_random_choice():
    i = random.randint(0,2)
    return options[i]


def get_user_input():
    return input("Rock, Paper, Scissors and Shoot: ").lower()


totalWins = 0
totalLoss = 0
totalTies = 0
totalGames = 1

userChoice = get_user_input()

while userChoice == "rock" or userChoice == "paper" or userChoice =="scissors":
    computerChoice = generate_random_choice()
    print("You chose " + userChoice + " and the computer chose " + computerChoice + ".")
    if userChoice=="rock":
        if computerChoice=="scissors":
            print("You win!")
            totalWins+=1
        elif computerChoice=="paper":
            print("You lose!")
            totalLoss+=1
        else:
            print("You tied! Rematch!")
            totalTies+= 1
    elif userChoice=="paper":
        if computerChoice=="rock":
            print("You win!")
            totalWins+=1
        elif computerChoice=="scissors":
            print("You lose!")
            totalLoss+=1
        else:
            print("You tied! Rematch!")
            totalTies += 1
    else:
        if computerChoice == "scissors":
            print("You win!")
            totalWins += 1
        elif computerChoice == "rock":
            print("You lose!")
            totalLoss += 1
        else:
            print("You tied! Rematch!")
            totalTies += 1

    print("So far, you played", totalGames, "games. You won ", totalWins, ", lost ", totalLoss, ", and tied", totalTies, "!")
    print("Play again!")

    totalGames += 1
    userChoice = get_user_input()
else:
    print("There was an error. Please refresh and try again :) ")